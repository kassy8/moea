<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Template Mockup</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">
  <link href="https://fonts.googleapis.com/css?family=Noto+Sans&display=swap" rel="stylesheet"> 
  <link rel="stylesheet" href="styles.template.css">
  <link rel="stylesheet" href="styles.template.animations.css">
  <link rel="stylesheet" href="styles.template.breakpoints.css">
</head>

<body>



<header>
    <nav class="container-row">
        <div class="pre-nav">
            <a href="tel:08000100100"><i class="icon-phone"></i> 08000 100 100</a>
        </div>
        <div class="main-nav">
            <div class="logo-holder">
                <a class="logo" href="#">
                    <h1>MyOnlineEstateAgent.com</h1>
                </a>
            </div>
            <ul>
                <li>
                    <a href="#">Sell</a>
                </li>
                <li>
                    <a href="#">Let</a>
                </li>
                <li>
                    <a href="#">Buy</a>
                </li>
                <li>
                    <a href="#">Why Use Us?</a>
                </li>
                <li class="menu-item-has-children">
                    <a href="#">How it Works</a>
<ul class="sub-menu">
	<li id="menu-item-770" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-770"><a href="http://moeawp2.tdrstaging.co.uk/how-we-work/">How we work</a></li>
	<li id="menu-item-322" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-322"><a href="http://moeawp2.tdrstaging.co.uk/how-we-work/choose-your-own-package/">Choose your own package</a></li>
	<li id="menu-item-899" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-899"><a href="http://moeawp2.tdrstaging.co.uk/how-we-work/immersive-video-tour/">Interactive Virtual Tour</a></li>
	<li id="menu-item-324" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-324"><a href="http://moeawp2.tdrstaging.co.uk/how-we-work/mortgage-advice/">Mortgage advice</a></li>
	<li id="menu-item-784" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784"><a href="http://moeawp2.tdrstaging.co.uk/how-we-work/conveyancing/">Conveyancing</a></li>
	<li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a href="http://moeawp2.tdrstaging.co.uk/about-moea/">About us</a></li>
	<li id="menu-item-287" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-287"><a href="http://moeawp2.tdrstaging.co.uk/faqs/">FAQs</a></li>
	<li id="menu-item-771" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-771"><a href="http://moeawp2.tdrstaging.co.uk/hints-and-tips/">Hints and Tips</a></li>
	<li id="menu-item-2479" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2479"><a href="http://moeawp2.tdrstaging.co.uk/how-to-be-a-good-landlord/">How to be a good landlord</a></li>
</ul>
                </li>
                <li>
                    <a class="moeabutton red" href="#">Free Valutation</a>
                </li>
                <li>
                    <a class="moeabutton white" href="#">My Account</a>
                </li>
            </ul>
        </div>
    </nav>    
</header>

<div class="header-area">
    <div class="container-row">
        <div class="splash-header">
            <div class="left-content">
                <h1>Get on Rightmove and Zoopla for just £1</h1>
                <h2>List your house in front of 98% of all online buyers today</h2>
                <ul class="usps">
                    <li>Get listed on Rightmove and Zoopla within 12 hours</li>
                    <li>You control of your house sale with unlimited updates</li>
                    <li>We assign a property expert throughout your sales journey</li>
                </ul>
                <ul class="credits">
                    <li>
                        <a href="#" class="moeabutton red">Get Listed for just &pound;1</a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            <img src="./assets/Rightmove_Logo_W.svg" alt="" />
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            <img src="./assets/Zoopla_Logo_W.svg" alt="" />
                        </a>
                    </li>
                </ul>
                <span>No Estate Agent Fees. No Commision</span>
            </div>
            <div class="right-content">
                <img src="1-pound-heart.png" alt="" title="" />
            </div>
        </div>    
    </div>
    <div class="heart-shape"></div>
</div>



starts<br>here<br>fam<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>



    <script src="core.template.js"></script>
    <script src="core.template.animations.js"></script>
</body>
</html>