var headernav = document.getElementsByTagName("header")[0];

document.body.onscroll = function() {
    console.log("scrolled to Y:" + window.scrollY);
    if (window.scrollY > 10) {
        headernav.classList.add('moea-sticky');
    } else {
        headernav.classList.remove('moea-sticky');
    }
};